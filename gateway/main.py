import logging
import random
from typing import Any

import requests
from fastapi import FastAPI

# Setup logs to warning by default.
# This, avoid to generate to much logs from 3rd parties libraries.
logging.basicConfig(level=logging.INFO)
# Then setup our module to DEBUG, to populate our logs with all informations.
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

app = FastAPI()
RPC_ENDPOINT_PROXY = "http://test-http-proxy-1:6000/jrpc"


def handle_response_from_proxy(response) -> dict[str, Any]:
    response.raise_for_status()
    response_data = response.json()

    if "error" in response_data:
        raise Exception(response_data["error"])
    else:
        return response_data


def jrpc_to_http_proxy(data: dict[str, Any]) -> dict[str, Any]:
    request = {
        "jsonrpc": "2.0",
        "id": random.randint(1, 2000),
        "method": "create",
        "params": data
    }
    response = requests.post(
        RPC_ENDPOINT_PROXY, json=request
    )

    return handle_response_from_proxy(response)


@app.get("/create")
async def create():
    logger.info("Call received from Client.")
    hardcoded_data = {"owner_id": "1234567890", "alias": "coucou"}

    # Send data to proxy via HTTP.
    request_ = jrpc_to_http_proxy(hardcoded_data)
    response = request_.get("result", {})
    logger.info("Response: %s", request_)

    return response
