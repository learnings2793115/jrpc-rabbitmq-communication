import json
import logging
from typing import Any, Optional
from dataclasses import dataclass

import requests
import uuid
from fastapi import FastAPI
from pydantic import BaseModel
from pika import (
    BasicProperties, BlockingConnection, ConnectionParameters, PlainCredentials
)
from pika.adapters.blocking_connection import BlockingChannel
from pika.frame import Method

# Setup logs to INFO by default, to keep logs from FastAPI.
logging.basicConfig(level=logging.INFO)
# But we do not need everything from pika.
logger = logging.getLogger("pika")
logger.setLevel(logging.WARNING)
# Then setup our module to DEBUG, to populate our logs with all informations.
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

app = FastAPI()


### JRPC between PROXY && GATEWAY ###
class JRPCRequestObject(BaseModel):
    jsonrpc: str
    id: int
    method: str
    params: dict[str, Any]


def format_rpc_response(request_id: int, result: dict[str, Any]) -> dict[str, Any]:
    response = {
        "jsonrpc": "2.0",
        "id": request_id,
        "result": result
    }
    return response


### RABBITMQ between PROXY && INTEL ###
RMQ_EXCHANGE = ""
RMQ_PUBLISH_QUEUE = "proxy_to_intel"
RMQ_RESPONSE_QUEUE = ""

RMQ_SERVER_IP = "169.254.50.9"
RMQ_SERVER_PORT = 5672
RMQ_CREDENTIALS = PlainCredentials("admin", "admin")
RMQ_VHOST = "test_vhost"

RMQ_PARAMETERS = ConnectionParameters(
    RMQ_SERVER_IP,
    RMQ_SERVER_PORT,
    RMQ_VHOST,
    RMQ_CREDENTIALS,
    heartbeat=60,
)

@dataclass
class RabbitMQClient:
    """Class to communicate with intel."""
    def __post_init__(self):
        self.connection: BlockingConnection = BlockingConnection(RMQ_PARAMETERS)
        self.channel: BlockingChannel = self.connection.channel()
        self.result: Method = self.channel.queue_declare(
            durable=True,
            queue=RMQ_RESPONSE_QUEUE,
            exclusive=True,
        )
        self.callback_queue: str = self.result.method.queue
        self.response: Optional[bytes] = None
        self.correlation_id: Optional[str] = None

        logger.info("RMQ Connection informations: ")
        logger.info("connection: %s", self.connection)
        logger.info("channel: %s", self.channel)
        logger.info("result: %s", self.result)
        logger.info("callback_queue: %s", self.callback_queue)

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.get_response,
        )

    def get_response(self, ch, method, props, body):
        if self.correlation_id == props.correlation_id:
            ch.basic_ack(delivery_tag=method.delivery_tag)
            self.response = body

    def call_to_intel(self, request_id, datas) -> Any:
        self.correlation_id = str(request_id)
        self.channel.basic_publish(
            exchange=RMQ_EXCHANGE,
            routing_key=RMQ_PUBLISH_QUEUE,
            properties=BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.correlation_id,
            ),
            body=datas,
        )
        self.connection.process_data_events(time_limit=None)
        return self.response


def decoded_response(response: Optional[bytes]) -> dict[str, Any]:
    if response is None:
        return {}
    return json.loads(response.decode('utf-8'))


def serialized(datas: JRPCRequestObject) -> str:
    return json.dumps(datas.__dict__)


### API exposes to GATEWAY ###
@app.post("/jrpc")
async def create(jrpc_request: JRPCRequestObject):
    logger.info("JRPC call received from Gateway.")
    logger.info("JRPCRequestObject: %s", jrpc_request)
    rmq_client = RabbitMQClient()
    request_id = jrpc_request.id

    # We must serialize JRPCRequestObject to send it throught
    # RabbitMQ then publish to Intel and wait response.
    response = rmq_client.call_to_intel(
        request_id=request_id, datas=serialized(jrpc_request)
    )

    logger.info("Response from intel: %s", response)

    return format_rpc_response(request_id=request_id, result=decoded_response(response))
