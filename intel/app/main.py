import json
import logging
from typing import Any
from dataclasses import dataclass

import pika
from pika import BlockingConnection, ConnectionParameters, PlainCredentials
from pika.adapters.blocking_connection import BlockingChannel
from pika.frame import Method


# Setup logs to warning by default.
# This, avoid to generate to much logs from 3rd parties libraries.
logging.basicConfig()
# Then setup our module to DEBUG, to populate our logs with all informations.
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

METHODS = {}

RMQ_SERVER_IP = "169.254.50.9"
RMQ_SERVER_PORT = 5672
RMQ_CREDENTIALS = PlainCredentials("admin", "admin")
RMQ_VHOST = "test_vhost"

RMQ_PARAMETERS = ConnectionParameters(
    RMQ_SERVER_IP,
    RMQ_SERVER_PORT,
    RMQ_VHOST,
    RMQ_CREDENTIALS,
    heartbeat=60,
)
RMQ_EXCHANGE = ""
RMQ_LISTENING_QUEUE = "proxy_to_intel"


### JRPC Object sent by proxy. ###
@dataclass
class JRPCRequestObject:
    jsonrpc: str
    id: int
    method: str
    params: dict[str, Any]


@dataclass
class RabbitMQClient:
    """Class to handle RMQ request from proxy."""
    def __post_init__(self):
        self.connection: BlockingConnection = BlockingConnection(RMQ_PARAMETERS)
        self.channel: BlockingChannel = self.connection.channel()
        self.result: Method = self.channel.queue_declare(queue=RMQ_LISTENING_QUEUE)

        logger.info("Connected to RMQ.")
        logger.info("Host: %s", RMQ_SERVER_IP)
        logger.info("Result: %s", self.result)

        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(
            on_message_callback=self.get_request_from_proxy,
            queue=RMQ_LISTENING_QUEUE,
        )

    def get_request_from_proxy(self, channel, method, props, body):
        logger.info("Body received from proxy: %s", body)
        response = self.execute_call(data_obj=deserialized(body))

        logger.info("Sending response to proxy: %s", response)
        channel.basic_ack(delivery_tag=method.delivery_tag)
        self.send_response_to_proxy(
            channel_to_publish=channel,
            response=response,
            properties=props,
        )

    def send_response_to_proxy(
            self, channel_to_publish, response: bytes, properties
    ):
        channel_to_publish.basic_publish(
            exchange=RMQ_EXCHANGE,
            routing_key=properties.reply_to,
            properties=pika.BasicProperties(correlation_id=properties.correlation_id),
            body=response,
        )

    def execute_call(self, data_obj: JRPCRequestObject) -> bytes:
        logger.info("Call to intel...")

        # Retrieve necessary informations.
        method_name: str = data_obj.method
        params: dict[str, Any] = data_obj.params

        # Execute method in call.
        if method_name in METHODS:
            response = METHODS[method_name](**params)
        else:
            raise Exception("Method %s not implemented" % method_name)

        return self.encode_response(response)

    def encode_response(self, response: dict[str, Any]) -> bytes:
        """Body must be in bytes for rabbitmq."""
        return json.dumps(response, indent=2).encode('utf-8')

    def starts(self):
        logger.info("Intel starts consumme...")
        try:
            self.channel.start_consuming()
        except KeyboardInterrupt:
            self.channel.stop_consuming()


def deserialized(datas: str) -> JRPCRequestObject:
    result = json.loads(datas)
    logger.info("Deserialized body: %s", result)
    return JRPCRequestObject(**result)


def create(owner_id: str, alias: str) -> dict[str, Any]:
    logger.info("Creating resource...")
    return {"owner_id": owner_id, "alias": alias}


def main():
    global METHODS
    logger.info("Starting intel...")

    METHODS.update({"create": create})

    rmq_client = RabbitMQClient()
    rmq_client.starts()


if __name__ == "__main__":
    main()
