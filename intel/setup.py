from setuptools import find_packages, setup

VERSION = "0.0.0"


setup(
    name="intel",
    version=VERSION,
    packages=find_packages(),
    description="RMQ POC.",
    install_requires=["pika==1.3.1"],
    entry_points={
        "console_scripts": [
            "starts-intel=app.main:main",
        ]
    },
)

